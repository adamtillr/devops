##Specify the name of the resource group
$ResourceGroupName = "Group"

##Login to Azure RM account
Login-AzureRmAccount -Credential
Get-AzureRmResource | Where-Object Name -EQ $ResourceGroupName 
Get-AzureRmResourceGroupDeployment -ResourceGroupName Group
Get-AlertRule -ResourceGroup $ResourceGroupName

##Create Alerts

$AlertEmail = "demosupport@tillr.io"
$WebTestsNames = @("attachments", "credentials", "files", "forms", "itops", "roles", "tasks", "tenants", "users", "emailservice", "rules", "api", "demo-hq-tillr", "demo-my-tillr")
$WebEndpoints = @("https://demoattachments.tillr.io/status", "https://democredentials.tillr.io/status", "https://demofiles.tillr.io/status", "https://demoforms.tillr.io/status", "https://demoitops.tillr.io/status", 
                   "https://demoroles.tillr.io/status", "https://demotasks.tillr.io/status", "https://demotenants.tillr.io/status", "https://demousers.tillr.io/status", "https://demoemailservice.tillr.io/status",
                   "https://demorules.tillr.io/status", "https://demoapi.tillr.io/status", "https://demohq.tillr.io", "https://demomy.tillr.io")

For($I=0;$I -lt $WebTestsNames.Count;$I++){

#New-AzureRMResourceGroupDeployment -ResourceGroupName $ResourceGroupName  -TemplateFile C:\URLCheck2.json -appName DEMO -webTestName $WebTestsNames[$i] -URL $WebEndpoints[$i] -Email "demosupport@tillr.io"
$deployment = New-AzureRMResourceGroupDeployment -ResourceGroupName $ResourceGroupName -Name DEMO -TemplateFile C:\URLCheck2.json -appName DEMO -webTestName Testing -URL http://adamcunningham.uk -Email "demosupport@tillr.io"

}