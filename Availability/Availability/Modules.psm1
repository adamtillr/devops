function Read-ConfigVariables
{
    Param (
        [Parameter(Mandatory=$true)]
        [String] $environmentDir
    )
    Process 
    {
        $variablesConfig = $environmentDir
		
		If (-not(Test-Path "$variablesConfig"))
		{
			"Variables configuration not found: $variablesConfig"
		}
		else
		{
			$variables = @{}
			$variablesXml = [xml](cat $variablesConfig)
			$variablesXml.configuration.variables.variable | foreach { 
            $variables.add($_.name, $_.value)
        }

			return $variables
		} 
        
    }
}