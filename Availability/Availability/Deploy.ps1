$baseDir = resolve-path .\.
$modules = Join-Path $baseDir "Modules.psm1"
$environmentDir = Join-Path $baseDir "Variables.config"

Import-Module $modules

$configVariables = Read-ConfigVariables $environmentDir

Login-AzureRmAccount
Select-AzureRmSubscription -SubscriptionName $configVariables.Subscription

#Location has to be centralus while AppInsights is in preview. 
New-AzureRmResourceGroup -Name $configVariables.ResourceGroupName -Location "centralus" -Force

$WebTestsNames = @("attachments", "credentials", "files", "forms", "itops", "roles", "tasks", "tenants", "users", "emailservice", "rules", "api", "hq-tillr", "my-tillr")
$WebEndpoints = @("https://attachments.tillr.io/status", "https://credentials.tillr.io/status", "https://files.tillr.io/status", "https://forms.tillr.io/status", "https://itops.tillr.io/status", 
                   "https://roles.tillr.io/status", "https://tasks.tillr.io/status", "https://tenants.tillr.io/status", "https://users.tillr.io/status", "https://emailservice.tillr.io/status",
                   "https://rules.tillr.io/status", "https://api.tillr.io/status", "https://hq.tillr.io", "https://my.tillr.io")

For($I=0;$I -lt $WebTestsNames.Count;$I++){
New-AzureRMResourceGroupDeployment -ResourceGroupName $configVariables.ResourceGroupName -Name "tillr-availability-production" -TemplateUri "https://tlroidemo.blob.core.windows.net/tillrappinsight/EnableAlert.json" -appName $configVariables.AppName -webTestName $WebTestsNames[$i] -URL $WebEndpoints[$i] -Email $configVariables.AlertEmail
}