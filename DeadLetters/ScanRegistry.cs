﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Reflection;
using Newtonsoft.Json;

namespace ServiceBusScanner
{
    public class ScanRegistry : IDisposable
    {
        private const string FileName = "QueueScanRegistry.json";
        private Dictionary<string, long> _lastSequenceNumberLookup;

        public ScanRegistry()
        {
            _lastSequenceNumberLookup = Read();
        }

        public void UpdateLastSequenceNumber(string queueName, long sequenceNumber)
        {
            var key = queueName.ToLower();
            if (_lastSequenceNumberLookup.ContainsKey(key))
            {
                _lastSequenceNumberLookup[queueName] = sequenceNumber;
            }
            else
            {
                _lastSequenceNumberLookup.Add(queueName, sequenceNumber);
            }
        }

        public long GetLastSequenceNumber(string queueName)
        {
            var key = queueName.ToLower();
            if (!_lastSequenceNumberLookup.ContainsKey(key))
            {
                return 0;
            }
            return _lastSequenceNumberLookup[key];
        }

        public void Save()
        {
            var filePath = GetFilePath();
            var lookupJson = JsonConvert.SerializeObject(_lastSequenceNumberLookup);
            File.WriteAllText(filePath, lookupJson);
        }

        private static Dictionary<string, long> Read()
        {
            var filePath = GetFilePath();

            if (!File.Exists(filePath))
            {
                return new Dictionary<string, long>();
            }

            var lookupJson = File.ReadAllText(filePath);
            if (string.IsNullOrWhiteSpace(lookupJson))
            {
                return new Dictionary<string, long>();
            }
            return JsonConvert.DeserializeObject<Dictionary<string, long>>(lookupJson);
        }

        private static string GetFilePath()
        {
            var path = Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location);
            return Path.Combine(path, FileName);
        }

        public void Dispose()
        {
            Save();
        }
    }
}
