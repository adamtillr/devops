﻿using System;
using System.Configuration;
using Topshelf;

namespace ServiceBusScanner
{
    public class Program
    {
        static void Main(string[] args)
        {
            var environmentName = ConfigurationManager.AppSettings["EnvironmentName"];
            var host = HostFactory.New(configurator =>
            {
                configurator.Service<ScannerService>();
                configurator.SetServiceName(String.Format("{0}DeadLettersScanner", environmentName));
                configurator.SetDisplayName(String.Format("{0} DeadLetters Scanner", environmentName));
                configurator.SetDescription(String.Format("{0} DeadLetters Scanner", environmentName));
                configurator.RunAsLocalSystem();
            });
            host.Run();
        }
    }
}
