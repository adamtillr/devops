﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Timers;
using Microsoft.ServiceBus;
using Microsoft.ServiceBus.Messaging;
using Topshelf;

namespace ServiceBusScanner
{
    public class ScannerService : ServiceControl
    {
        private readonly Timer scanTimer;

        public ScannerService()
        {
            scanTimer = new Timer();
        }

        private int ScanInterval
        {
            get
            {
                return int.Parse(ConfigurationManager.AppSettings["ScanInterval"]) * 1000;
            }
        }

        public bool Start(HostControl hostControl)
        {
            scanTimer.Interval = ScanInterval;
            scanTimer.Elapsed += new System.Timers.ElapsedEventHandler(this.OnTimer);
            scanTimer.Start();
            return true;
        }

        private void OnTimer(object sender, ElapsedEventArgs e)
        {
            scanTimer.Stop();
            using (var scanRegistry = new ScanRegistry())
            {
                ScanQueues(scanRegistry);
            }
            scanTimer.Interval = ScanInterval;
            scanTimer.Start();
        }

        private void ScanQueues(ScanRegistry scanRegistry)
        {
            string connectionString = ConfigurationSettings.AppSettings.Get("Microsoft.ServiceBus.ConnectionString");

            string url = ConfigurationSettings.AppSettings.Get("Slack.Url");

            string channel = ConfigurationSettings.AppSettings.Get("Slack.Channel");

            Console.WriteLine("Settings: " + " " + url + " " + channel);

            var nameSpace = NamespaceManager.CreateFromConnectionString(connectionString);

            IEnumerable<QueueDescription> queueList = nameSpace.GetQueues("messageCount Gt 0");

            foreach (QueueDescription qd in queueList)
            {
                var queueName = qd.Path;
                if (qd.MessageCount > 0 && qd.Path.Contains("error"))
                {
                    Console.WriteLine("Reading Error messages from: {0}", qd.Path);

                    QueueClient client = QueueClient.CreateFromConnectionString(connectionString, qd.Path);

                    var receivedMessages = client.PeekBatch(250);

                    if (receivedMessages.Count() == 0)
                    {
                        Console.WriteLine("No messages...");
                        continue;
                    }

                    var lastSeqNo = scanRegistry.GetLastSequenceNumber(queueName);
                    long newLastSeqNo = lastSeqNo;
                    foreach (var receivedMessage in receivedMessages)
                    {
                        if (receivedMessage.SequenceNumber <= lastSeqNo)
                        {
                            continue;
                        }

                        newLastSeqNo = receivedMessage.SequenceNumber;
                        var json = ReadBody(receivedMessage);

                        Slack slackClient = new Slack(url);
                        slackClient.PostMessage(username: "ServiceBusBot",
                                           text: "Error Queue: " + qd.Path + "\n Message Body: " + json +
                                           "\n Exception Type: " + receivedMessage.Properties["NServiceBus.ExceptionInfo.ExceptionType"] +
                                           "\n Exception Message: " + receivedMessage.Properties["NServiceBus.ExceptionInfo.Message"] +
                                           "\n Exception StackTrace: " + receivedMessage.Properties["NServiceBus.ExceptionInfo.StackTrace"],
                                           channel: channel);
                    }
                    scanRegistry.UpdateLastSequenceNumber(queueName, newLastSeqNo);
                    Console.WriteLine("Checked Production Services");
                }
            }
        }

        private static string ReadBody(BrokeredMessage message)
        {
            var body = message.GetBody<byte[]>();
            using (var reader = new StreamReader(new MemoryStream(body)))
            {
                return reader.ReadToEnd();
            }
        }

        public bool Stop(HostControl hostControl)
        {
            scanTimer.Stop();
            return true;
        }
    }
}
